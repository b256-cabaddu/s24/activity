let getCube = 2 ** 3;
console.log(`The cube of ${2} is ${getCube}`)

let houseAddress = {
	houseNumber: 258,
	street: "Washington Ave",
	direction: "NW", 
	state: "California", 
	postalCode: 90011
}

let {houseNumber,street, direction, state, postalCode} = houseAddress

console.log(`I live at ${houseNumber} ${street} ${direction}, ${state} ${postalCode}`)

let animal = {
	name: 'Lolong',
	type: 'saltwater',
	weigth: 1075,
	measurement: "20 ft 3 in"
}

let {name, type, weight, measurement} = animal
console.log(`${name} was a ${type} crocodile. He weighed at ${weight} with a measurement of ${measurement}`)

let numbers = [1, 2, 3, 4, 5, 15]
let number = numbers.forEach(num => console.log(num)) 

class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let dog1 = new Dog("Franke", 5, "Miniature Daschshund")
console.log(dog1)